class MemElement(object):
    """
    Represents a memory symbol (observation and reward, or action) that can be stored in the agent's memory
    The abstraction allows us to type-check memory elements to ensure that nodes are indexed correctly
    """
    ACTION = 0
    PERCEPT = 1

    def __init__(self, data, type):
        self.data = data
        assert(type == MemElement.ACTION or type == MemElement.PERCEPT)
        self.type = type

    def __hash__(self):
        return hash(self.data)


class Action(MemElement):
    def __init__(self, data):
        super(Action, self).__init__(data, type=MemElement.ACTION)


class Percept(MemElement):
    def __init__(self, data):
        super(Percept, self).__init__(data, type=MemElement.PERCEPT)
