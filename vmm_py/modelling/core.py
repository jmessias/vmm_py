import logging
import pydot
import numpy as np
import pdb
from abc import ABCMeta, abstractmethod

import vmm_py.modelling.estimators as estimators


MAX_EXPONENT = 100  # maximum useful exponent (also max value for beta)
MAX_EXP2 = 2**MAX_EXPONENT
MAX_PRINT_DEPTH = 7


class TreeNode(object):
    def __init__(self, parent=None, label=None):
        self._children = {}  # mem label -> node (TreeNode)
        self._parent = parent
        self._label = label  # could also be recovered from the node->parent edge
        self._marking = None  # for printing purposes
        if parent is None:
            self._depth = 0
        else:
            self._depth = parent.get_depth() + 1
        if self._parent is None:
            self._uid = str("R")
        else:
            self._uid = self._parent._uid + "-" + str(self._label)

    def __getitem__(self, m):
        return self.get_child(m)

    def add_child(self, m):
        self._children[m] = TreeNode(parent=self, label=m)

    def set_child(self, m, v):
        assert(issubclass(type(v), TreeNode))
        p = v.get_parent()
        if p is not None:
            assert(v.get_parent() == self)  # otherwise we're stealing someone else's child
        else:
            v.set_parent(self)
        self._children[m] = v

    def get_child(self, m):
        return self._children[m]

    def get_children(self):
        return self._children.items()

    def get_children_at_depth(self, maxdepth, depth=0):
        if depth >= maxdepth or self.is_leaf():
            return [self]
        else:
            gclist = []
            for c in self._children.values():
                gclist.extend(c.get_children_at_depth(maxdepth, depth+1))
            return gclist

    def get_parent(self):
        return self._parent

    def set_parent(self, p):
        self._parent = p
        self._depth = p.get_depth() + 1
        self._update_uid()

    def is_leaf(self):
        return len(self._children) == 0

    def set_depth(self, depth):
        self._depth = depth

    def get_depth(self):
        return self._depth

    def get_uid(self):
        return self._uid

    def get_leaves(self):
        if self.is_leaf():
            return [self]
        else:
            leaves = []
            for c in self._children.values():
                leaves.extend(c.get_leaves())
            return leaves

    def get_label(self):
        return self._label

    def _update_uid(self):
        if self._parent is None:
            self._uid = str("R")
        else:
            self._uid = self._parent._uid + "-" + str(self._label)

    def update_uid_full(self, max_depth):
        if self.get_depth() > max_depth:
            return
        """Use with care"""
        self._update_uid()
        for c in self._children.values():
            c.update_uid_full(max_depth)


class PredictorNode(TreeNode):
    def __init__(self, estimator, builder, parent=None, label=None):
        super(PredictorNode, self).__init__(parent, label)
        self._total_exp = 0
        self._counts = {}
        self._est = estimator
        self._builder = builder

    def add_child(self, m):
        ntype = type(self)
        v = ntype(estimator=self._est,
                  builder=self._builder,
                  parent=self,
                  label=m)
        self._children[m] = v
        self._builder.node_count += 1
        if self._builder.get_node_count() > self._builder.max_nodes > 0:
            logging.warning("Node count has exceeded specified max.")
        return v

    def add_exp(self, y):
        if y in self._counts:
            self._counts[y] += 1
        else:
            self._counts[y] = 1
        self._total_exp += 1

    def get_counts(self):
        return self._counts

    def get_known_ab(self):
        return self._counts.keys()

    def get_ab_size(self):
        return len(self.get_counts())

    def get_default_ab_size(self):
        return self._est.default_ab_size

    def get_node_size(self):
        raise NotImplementedError

    def get_total_exp(self):
        return self._total_exp

    def get_count(self, y):
        if y in self._counts:
            return self._counts[y]
        else:
            return 0

    def get_readable_pmf(self):
        total_exp = self.get_total_exp()
        pmf = []
        if total_exp > 0:
            counts = self.get_counts()
            for (y, c) in counts.items():
                pmf.append((y, c/float(total_exp)))
        return pmf

    def get_estimator(self):
        return self._est

    def is_deterministic(self):
        return (len(self.get_counts()) == 1)


class ContextTreeBuilder(object):
    __metaclass__ = ABCMeta

    def __init__(self, params, node_type):
        logging.info('Initializing ContextTreeBuilder with parameters:')
        if 'binary' in params and params['binary']:
            self._default_ab_size = 2
        else:
            try:
                self._default_ab_size = params['ab_size']
            except KeyError:
                self._default_ab_size = 128
        logging.info('Alphabet size: %d' % self._default_ab_size)

        assert 'estimator' in params, "You need to supply the 'estimator' parameter."
        est_str = params['estimator']
        assert est_str in estimators.estimator_labels, ("Estimator '%s' unsupported / "
                                                        "unknown. Available estimators are: %s" %
                                                        (est_str, estimators.estimator_labels.values()))
        est_type = estimators.estimator_labels[est_str]
        estimator = est_type(self._default_ab_size)
        self._root = node_type(estimator, self)
        logging.info('Estimator: %s' % est_str)

        try:
            self._max_depth = params['max_depth']
            logging.info('Max depth: %d' % self._max_depth)
        except KeyError:
            self._max_depth = 0

        try:
            self.max_nodes = params['max_nodes']
            logging.info('Max number of nodes: %d' % self.max_nodes)
        except KeyError:
            self.max_nodes = -1

        self._total_exp = 0
        self.node_count = 1

    @abstractmethod
    def train(self, y, stm):
        pass

    @abstractmethod
    def test(self, y, stm):
        pass

    def match_stm(self, stm):
        n = self._root
        for m in stm:
            try:
                n = n.get_child(m)
            except KeyError:
                break
        return n

    def sample(self, stm):
        known_ab = self._root.get_known_ab()
        total_p = 0
        rv = np.random.rand()
        if len(known_ab) > 1:
            for y in known_ab:
                p_y = self.test(y, stm)
                total_p += 2**-p_y
                if total_p > rv:
                    return y
            s = known_ab[-1]
        else:
            s = stm[-1][0]
        return s

    def get_utile_mem_length(self):
        return self._max_depth

    def print_tree(self, filepath):
        title = "Time = %d" % self._total_exp
        dag = pydot.Dot(title,
                        graph_type="digraph",
                        rankdir="TB",
                        ranksep="1.2 equally",
                        label=title,
                        dpi=300)

        self.build_pydot_dag(dag, self._root, "root", "root")

        dag.write(path=filepath, format='png')

    def build_pydot_dag(self, dag, node, idn, label=''):
        pmf = node.get_readable_pmf()
        if pmf is not None:
            pmf_str = " ".join('(%s, %0.2f)' % p for p in pmf)
        else:
            pmf_str = '--'
        full_label = ('<<FONT POINT-SIZE="8">p = [%s]<BR/>count = %d</FONT>>'
                      % (pmf_str, node.get_total_exp()))

        if node._marking is not None:
            color = node._marking
            node._marking = None
        else:
            color = 'black'

        n = pydot.Node(idn,
                       label=full_label,
                       style="filled",
                       color=color)
        dag.add_node(n)

        visited_nodes = set([])
        for (m, c) in node.get_children():
            if c not in visited_nodes:
                visited_nodes.add(c)
                new_idn = c.get_uid()
                label = c.get_label()
                e = pydot.Edge(idn,
                               new_idn,
                               label=label,
                               color='black')
                dag.add_edge(e)
                self.build_pydot_dag(dag, c, new_idn)

    def get_default_ab_size(self):
        return self._default_ab_size

    def get_root(self):
        return self._root

    def get_max_depth(self):
        return self._max_depth

    def get_node_count(self):
        return self.node_count

    def get_height(self):
        """DFS Search -- use with care"""
        nodelist = [self._root]
        maxdepth = 0
        while len(nodelist) > 0:
            n = nodelist.pop()
            d = n.get_depth()
            if d > maxdepth:
                maxdepth = d
            nodelist.extend(n._children.values())
        return maxdepth
