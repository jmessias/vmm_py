import numpy as np
import pdb
from abc import ABCMeta, abstractmethod

class Model(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def sample(self, history):
        raise NotImplementedError

class MLMarkov(Model):
    def __init__(self, params):
        self.transition_dict = {}
        self.total_counts = {}

    def train(self, y, history):
        # :param history: expected to follow the format [(o_1, r_1), a_1), (o_2, r_2), a_2), ...]
        if len(history) == 0:
            return
        ((s, r), a) = history[0]
        if (s, a) not in self.transition_dict:
            self.transition_dict[(s, a)] = {}
            self.total_counts[(s, a)] = 0
        if y not in self.transition_dict[(s, a)]:
            self.transition_dict[(s, a)][y] = 0
        self.transition_dict[(s, a)][y] += 1
        self.total_counts[(s, a)] += 1

    def sample(self, history):
        """
        :param history: expected to follow the format [(o_1, r_1), a_1), (o_2, r_2), a_2), ...]
        :return:
        """
        if len(history) == 0:
            return
        ((s, r), a) = history[0]
        if (s, a) not in self.transition_dict:
            if len(self.transition_dict) > 0:
                rv = np.random.choice(len(self.transition_dict.keys()))
                k = self.transition_dict.keys()[rv]  # state not known, assumes uniform pdf
                rv = np.random.choice(len(self.transition_dict[k].keys()))
                y = self.transition_dict[k].keys()[rv]
                return y
            else:
                return (s, r)

        rv = np.random.rand()
        tot_mass = 0
        for (y, c) in self.transition_dict[(s, a)].items():
            tot_mass += c/float(self.total_counts[(s, a)])
            if rv < tot_mass:
                return y

    def get_utile_mem_length(self):
        return 1

    def get_minimum_mem_length(self):
        return 1

