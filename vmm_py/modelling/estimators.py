import numpy as np
import numpy.matlib as npmat
import pdb
from abc import ABCMeta, abstractmethod
from scipy.special import gammaln
from scipy.special import betaln
from collections import Counter

log2c = np.log(2)


class CountBasedEstimator(object):
    __metaclass__ = ABCMeta
    """
    Lets you
    - Compute probabilities for a given label
    - Compute differences between pmfs based on absolute frequencies
    """
    @abstractmethod
    def get_prob(self, ny, ntot, nu):
        pass

    @abstractmethod
    def get_pmf(self, Ny, ntot, nu):
        pass


class SADEstimator(CountBasedEstimator):
    def __init__(self, default_ab_size):
        self.default_ab_size = default_ab_size

    def get_prob(self, ny, ntot, nu):
        if ntot > 0:
            beta = nu/np.log2((ntot + 1)/float(nu))
        else:
            beta = 1.0
        if ny == 0:
            w = 1.0/(self.default_ab_size - nu)
            p = beta*w/(ntot + beta)
        else:
            p = ny/(ntot + beta)
        assert(1 >= p >= 0)

        return p

    def get_pmf(self, Ny, ntot, nu):
        if ntot > 0:
            beta = nu/np.log2((ntot + 1)/float(nu))
        else:
            beta = 1.0
        p = Ny/(ntot + beta)
        if np.any(Ny == 0):
            w = 1.0/(self.default_ab_size - nu)
            pn = beta*w/(ntot + beta)
            p += pn*(Ny == 0)
            if ntot + beta <= 0:
                pdb.set_trace()
            assert(ntot + beta > 0)
        p = p/np.sum(p)
        return p


class SADMEstimator(CountBasedEstimator):
    def __init__(self, default_ab_size):
        self.default_ab_size = default_ab_size

    def get_prob(self, ny, ntot, nu):
        if ntot > 0:
            if ntot < nu:
                nu = ntot
            beta = nu/np.log2((ntot + 1)/float(nu))
        else:
            beta = 1.0
        """
        (n_i^t + (m_t/(D log ((t+1)/m_t))) / (t + (m_t/log ((t+1)/m_t)))
        """
        p = (ny + beta/self.default_ab_size)/(ntot + beta)
        assert(1 >= p >= 0)
        return p

    def get_seq_prob(self, Ny, ntot, nu):
        raise NotImplementedError

    def get_counts_logprob(self, cdict, ctot, ndict=Counter(), ntot=0):
        """
        DirMultinomial prob of cdict given pdict as a prior
        **conditioned on ctotal! Not the sequence prob**
        cdict: dictionary of counts
        ctotal: sum of cdict
        ndict: dictionary of prior counts
        ntot: sum of ndict

        Note that the prior on the alphas is:
        alpha_i = beta/self.default_ab_size
        """
        nu = min([len(cdict), ctot])  # note: this is an approximation, but we can't reliably know nu without wasting too much time or memory
        assert(nu > 0)
        if ctot > 0:
            beta = nu/np.log2((ctot + 1)/float(nu))
        else:
            beta = 1.0
        alpha_k = beta/self.default_ab_size
        sum_alpha = beta + ntot  # prior counts sum on the Dir. parameters
        logp = np.log(ctot) + betaln(sum_alpha, ctot)
        for (y, c_k) in cdict.items():
            if c_k <= 0:
                continue
            try:
                n_k = ndict[y]
            except KeyError:
                n_k = 0  # not going to happen if ndict is a Counter
            logp -= (np.log(c_k) + betaln(alpha_k + n_k, c_k))
        return logp/log2c

    def get_seq_logprob(self, cdict, ctot, ndict=Counter(), ntot=0):
        """
        DirMultinomial prob of cdict given pdict as a prior
        **Sequence prob: not conditioned on ctotal!**
        cdict: dictionary of counts
        ctotal: sum of cdict
        ndict: dictionary of prior counts
        ntot: sum of ndict

        Note that the prior on the alphas is:
        alpha_i = beta/self.default_ab_size
        """
        nu = min([len(cdict), ctot])  # note: this is an approximation, but we can't reliably know nu without wasting too much time or memory
        assert(nu > 0)
        if ctot > 0:
            beta = nu/np.log2((ctot + 1)/float(nu))
        else:
            beta = 1.0
        alpha_k = beta/self.default_ab_size
        sum_alpha = beta + ntot  # prior counts sum on the Dir. parameters
        logp = gammaln(sum_alpha) - gammaln(sum_alpha + ctot)
        for (y, c_k) in cdict.items():
            if c_k <= 0:
                continue
            try:
                n_k = ndict[y]
            except KeyError:
                n_k = 0  # not going to happen if ndict is a Counter
            logp += gammaln(alpha_k + n_k + c_k) - gammaln(alpha_k + n_k)
        return logp/log2c

    def get_pmf(self, Ny, ntot, nu):
        raise NotImplementedError


class MethodCEstimator(CountBasedEstimator):
    def __init__(self, default_ab_size):
        self.default_ab_size = default_ab_size

    def get_prob(self, ny, ntot, nu):
        if not ntot > 0:
            p_y = 1.0/self.default_ab_size
        elif ny > 0:
            p_y = float(ny)/(ntot + nu)
        else:
            p_y = float(nu)/(ntot + nu)
        if p_y == 0:
            pdb.set_trace()
        return p_y

    def get_pmf(self, Ny, ntot, nu):
        if not ntot > 0:
            return 1.0/self.default_ab_size*npmat.ones([self.default_ab_size, 1])
        else:
            sel = Ny == 0
            p_y = Ny/(ntot + nu)
            p_e = nu/(ntot + nu)
            return p_y + np.multiply(sel, p_e)


class KTEstimator(CountBasedEstimator):
    """
    The KT estimator assumes that the alphabet size is know a priori (non adaptive alphas)
    """
    def __init__(self, default_ab_size):
        self.default_ab_size = default_ab_size

    def get_prob(self, ny, ntot, nu):
        if ntot <= 0:
            return 1.0/self.default_ab_size
        p_y = (ny + 0.5)/(ntot + self.default_ab_size*0.5)
        return p_y

    def get_pmf(self, Ny, ntot, nu):
        if ntot <= 0:
            return (1.0/self.default_ab_size)*npmat.ones([self.default_ab_size, 1])

        p_y = (Ny + 0.5)/(ntot + nu*0.5)
        p_y = p_y/np.sum(p_y)
        return p_y

    def get_seq_logprob(self, Ny, ntot, nu, prior_counts=None):
        if ntot <= 0:
            return 1.0
        alpha = 0.5*np.ones(self.default_ab_size)  # Jeffreys prior
        sum_alpha = self.default_ab_size*0.5
        if prior_counts is not None:
            alpha += prior_counts  # this establishes the dirichelet posterior after observing prior_counts
            sum_alpha += np.sum(prior_counts)
        log_prod_nyai = 0.0
        log_prod_ai = 0.0
        for i in xrange(np.size(Ny)):
            ny = Ny[i, 0]
            log_prod_nyai += gammaln(ny + alpha[i])
            log_prod_ai += gammaln(alpha[i])
        delta = log_prod_nyai - log_prod_ai
        logprob = gammaln(sum_alpha) - gammaln(ntot+sum_alpha) + delta
        return logprob/log2c

estimator_labels = {'SAD': SADEstimator,
                    'SADM' : SADMEstimator,
                    'FREQ': MethodCEstimator,
                    'KT': KTEstimator}
