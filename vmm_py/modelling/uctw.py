import numpy as np
import logging
import pydot
import pdb

import vmm_py.modelling.core as core
from vmm_py.modelling.core import PredictorNode
from vmm_py.modelling.ctw import CTW


"""
UCTW: CTW with a USM-like fringe-expansion mechanism
"""


class UCTWNode(PredictorNode):
    def __init__(self, estimator, builder, parent=None, label=None):
        super(UCTWNode, self).__init__(estimator, builder, parent, label=str(label))
        self._beta = 0.0  # LOG of the LL ratio prod(Pw_ch)/Pe
        self._dbeta = 0.0 # offset on beta by fringe children.
        self._rho = 0.5  # "potential"
        self.is_frontier = False
        if parent is not None:
            self._rho *= parent.get_rho()
        self._subtree_size = 0

    def _reset_beta(self):
        """
        Used to reset this node back to its initial conditions (for example, after a pruning step)
        :return:
        """
        self._beta = 0.0
        self._dbeta = 0.0
        self._rho = 0.5  # "potential"

    def get_rho(self):
        return self._rho

    def get_subtree_size(self):
        return self._subtree_size

    def add_child(self, m):
        v = super(UCTWNode, self).add_child(m)
        if v.get_depth() > self._builder._height:
            self._builder._height = v.get_depth()
        self._builder.fringe_node_count += 1
        return v

    def rem_child(self, m):
        del self._children[m]

    def get_fringe_size(self):
        fringe_size = 0
        for _, c  in self.get_children():
            if c.is_frontier:
                fringe_size += 1 + c.get_subtree_size()
        return fringe_size

    def update_weighted_prob(self, y, stm, add_exp=True, fringe_depth=0):
        ny = self.get_count(y)
        pe_cond = self.get_estimator().get_prob(ny, self.get_total_exp(), self.get_ab_size())
        delta_log_pe = np.log2(pe_cond)

        delta_size = 0
        d = self.get_depth()
        self._rho_update()

        if d >= len(stm):
            delta_log_pw = delta_log_pe
        else:
            m = stm[d]
            dbeta = 0.0
            delta_log_pw_children = 0.0
            child_available = False

            if add_exp:
                if m not in self._children:
                    create_cond = (fringe_depth < self._builder.max_fringe_depth)
                    if create_cond:
                        c = self.add_child(m)
                        if fringe_depth == 0:
                            c.is_frontier = True
                        else:  # already in the fringe, that is ok
                            self._subtree_size += 1
                            delta_size += 1

                if m in self._children:
                    c = self._children[m]
                    if c.is_frontier or fringe_depth > 0:
                        fringe_depth += 1

                    (delta_log_pw_children, delta_subtree_size) = c.update_weighted_prob(y, stm, add_exp, fringe_depth)

                    if c.is_frontier:  # frontier case (the children are the frontier, but the parent gets the dbeta)
                        child_available = False
                        self._dbeta += delta_log_pw_children - delta_log_pe
                        self.test_expansion()
                    else:  # "normal" update
                        child_available = True
                        dbeta = delta_log_pw_children - delta_log_pe
                        delta_size += delta_subtree_size
                        self._subtree_size += delta_subtree_size
            else:
                if m in self._children:
                    c = self._children[m]
                    if not c.is_frontier:
                        child_available = True
                        (delta_log_pw_children, _) = c.update_weighted_prob(y, stm, add_exp)
                        dbeta = delta_log_pw_children - delta_log_pe

            if not child_available:  # We prevent updates in the fringe from carrying over into the proper tree
                delta_log_pw = delta_log_pe
            else:
                b = self._beta + dbeta
                beta_post = b if b < core.MAX_EXPONENT else core.MAX_EXPONENT
                if beta_post <= -core.MAX_EXPONENT:
                    delta_log_pw = delta_log_pe
                elif beta_post >= core.MAX_EXPONENT:
                    delta_log_pw = delta_log_pw_children
                else:
                    f = (1 + 2 ** beta_post) / (1 + 2 ** self._beta)
                    delta_log_pw = delta_log_pe + np.log2(f)

                if add_exp:
                    self._beta = beta_post
                    if fringe_depth == 0 and self._builder.enable_pruning:
                        self.test_prune()

        if add_exp:
            self.add_exp(y)

        return delta_log_pw, delta_size

    def test_expansion(self):
        if self.get_total_exp() > self._builder.min_exp and self._dbeta > 0 and self._beta + self._dbeta > 0:
            fringe_size = self.get_fringe_size()
            # Fringe expansion value computation (expected LL ratio at the root)
            ll_value_expand = self.get_expand_val()
            if ll_value_expand > self._builder.expand_lvl:
                self._builder.register_expansion(self, ll_value_expand, fringe_size)

    def get_expand_val(self):
        alpha_expand = (1 + 2 ** self._dbeta) / 2.0 if self._dbeta < core.MAX_EXPONENT else core.MAX_EXP2 / 2.0
        ll_value_expand = 1.0 + self._rho * (alpha_expand - 1.0)
        return ll_value_expand

    def test_prune(self):
        n_exp = self._builder.get_best_expansion()
        if (self.get_total_exp() > self._builder.min_exp and
            n_exp is not None and
            n_exp.mem_val > self._builder.get_available_size()):  # consider pruning only if the best expansion costs more than the available mem
            """Compute the pruning utility"""
            if self._beta <= -core.MAX_EXPONENT:
                alpha_prune = 1.0
            elif self._beta < core.MAX_EXPONENT:
                alpha_prune = 2.0 / (1 + 2 ** self._beta)
            else:
                alpha_prune = 2.0 / (1 + core.MAX_EXP2)
            ll_cost_prune = 1.0 + self._rho * (alpha_prune - 1.0)
            self._builder.register_prune(self, ll_cost_prune, self._subtree_size)

    def prune_children(self):
        dbeta = -self._beta if self._beta > 0.0 else 0.0
        self._reset_beta()
        for c in self._children.values():
            c._parent = None
        self._children = {}
        try:
            dsize = -self._subtree_size
            self._post_update_hook(dbeta, dsize)
            self._subtree_size = 0
            return dsize
        except Exception as e:
            return 0.0

    def _beta_update(self, log_alpha):
        if log_alpha != 0:
            log_alpha_post = np.log2((1 + 2 ** (self._beta + log_alpha)) / (1 + 2 ** self._beta))
            b = self._beta + log_alpha
            self._beta = b if b < core.MAX_EXPONENT else core.MAX_EXPONENT
            log_alpha = log_alpha_post
        return log_alpha

    def _rho_update(self):
        """
        Note: you can't cap rho to 0.5 if beta < 0.5, because rho will be used to eval potential fringe expansions
        if those expansions happen below a node with beta < 0, you need to weigh them accordingly
        :return:
        """
        # 1 / (1 + exp(-beta))
        if self._beta >= core.MAX_EXPONENT:
            rho_n = 1.0
        elif self._beta <= -core.MAX_EXPONENT:
            rho_n = 0.0
        else:
            rho_n = 1.0 / (1.0 + 2 ** -self._beta)
        p = self.get_parent()
        if p is not None:
            if p.get_rho() < 1e-6 or rho_n < 1e-6:
                self._rho = 0.0
            else:
                self._rho = rho_n * p.get_rho()
        else:
            self._rho = rho_n

    def _post_update_hook(self, log_alpha, dsize):
        # Returns the delta at the root
        if self.is_frontier:
            logging.error("Fringe node update carried over into internal nodes.")
        self._subtree_size += dsize
        if log_alpha != 0:
            log_alpha = self._beta_update(log_alpha)
        p = self.get_parent()
        if p is not None:
            return p._post_update_hook(log_alpha, dsize)
        else:
            if self.get_depth() == 0:
                return log_alpha
            else:
                logging.debug("Post-update called on disconnected tree. Skipping.")
                raise Exception()

    def expand_fringe(self, mcost):
        for _, c in self.get_children():
            c.is_frontier = False
        self._post_update_hook(self._dbeta, mcost)
        self._dbeta = 0


class ExpansionVal(object):
    PREF_MIXED = 0
    PREF_LL = 1

    def __init__(self, node, ll_val, mem_val, pref=PREF_MIXED):
        self.ll_val = ll_val
        self.mem_val = mem_val
        self.node = node
        assert(pref == ExpansionVal.PREF_MIXED or pref == ExpansionVal.PREF_LL)
        self.pref = pref

    def __eq__(self, other):
        return self.ll_val == other.ll_val and self.mem_val == other.mem_val

    def __ne__(self, other):
        return self.ll_val != other.ll_val or self.mem_val != other.mem_val


    # todo: fix these as they are different for splits and prunes
    def __lt__(self, other):
        if self.pref == ExpansionVal.PREF_MIXED:
            return self.ll_val/float(self.mem_val) < other.ll_val/float(other.mem_val)
        else:
            return self.ll_val < other.ll_val

    def __le__(self, other):
        if self.pref == ExpansionVal.PREF_MIXED:
            return self.ll_val/float(self.mem_val) <= other.ll_val/float(other.mem_val)
        else:
            if self.ll_val == other.ll_val:
                return self.mem_val >= other.mem_val
            else:
                return self.ll_val < other.ll_val

    def __gt__(self, other):
        if self.pref == ExpansionVal.PREF_MIXED:
            return self.ll_val/float(self.mem_val) > other.ll_val/float(other.mem_val)
        else:
            return self.ll_val > other.ll_val

    def __ge__(self, other):
        if self.pref == ExpansionVal.PREF_MIXED:
            return self.ll_val/float(self.mem_val) >= other.ll_val/float(other.mem_val)
        else:
            if self.ll_val == other.ll_val:
                return self.mem_val <= other.mem_val
            else:
                return self.ll_val > other.ll_val


class UCTW(CTW):
    def __init__(self, params):
        if 'max_depth' in params:
            logging.warn("Clearing the 'max_depth' argument (not used in UCTW)")
            del params['max_depth']

        super(UCTW, self).__init__(params,
                                   nodetype=UCTWNode)

        exp_lvl_def = 1.1
        try:
            self.expand_lvl = params['split_lvl']
        except KeyError:
            self.expand_lvl = exp_lvl_def
        if not self.expand_lvl >= 1.0:
            logging.error('Invalid likelihood ratio for fringe expansion. Reverting to default (%.2f).' % exp_lvl_def)
            self.expand_lvl = exp_lvl_def
        logging.info('Fringe Expansion LH Ratio Threshold: %.3f' % self.expand_lvl)

        try:
            self.max_fringe_depth = params['max_fringe_depth']
        except KeyError:
            self.max_fringe_depth = 2
        logging.info('Maximum fringe depth: %d' % self.max_fringe_depth)

        try:
            self.min_exp = params['min_exp']
            logging.info('Minimum node experience count: %d' % self.min_exp)
        except KeyError:
            self.min_exp = 0

        try:
            self.enable_pruning = params['enable_pruning']
        except KeyError:
            self.enable_pruning = True
        logging.info('Enable pruning: %s' % self.enable_pruning)

        self._height = 0
        self.fringe_node_count = 0
        self.best_prune = None
        self.best_expansion = None

    def register_expansion(self, node, ll_val, mem_val):
        exp = ExpansionVal(node, ll_val, mem_val)
        if self.best_expansion is None:
            self.best_expansion = exp
        else:
            if exp.ll_val > self.best_expansion.ll_val:
                self.best_expansion = exp

    def get_best_expansion(self):
        return self.best_expansion

    def register_prune(self, node, ll_val, mem_val):
        prune = ExpansionVal(node, ll_val, mem_val)
        if self.best_prune is None:
            self.best_prune = prune
        else:
            if prune.ll_val > self.best_prune.ll_val:  # higher ll means the prune does less damage
                self.best_prune = prune

    def get_best_prune(self):
        return self.best_prune

    def train(self, y, stm):
        if len(stm) < self.get_minimum_mem_length():
            loss = -np.log2(1.0/self.get_default_ab_size())
        else:
            (loglikeh,_) = self.get_root().update_weighted_prob(y, stm, add_exp=True)
            loss = -loglikeh
            assert(loss >= 0)
        self._total_exp += 1
        self.post_process()
        return loss

    def test(self, y, stm):
        if len(stm) < self.get_minimum_mem_length():
            loss = -np.log2(1.0/self.get_default_ab_size())
        else:
            (loglikeh,_) = self.get_root().update_weighted_prob(y, stm, add_exp=False)
            loss = -loglikeh
            assert(loss >= 0)
        return loss

    def get_available_size(self):
        if self.max_nodes > 0:
            return self.max_nodes - self.get_node_count()
        else:
            return np.Inf

    def is_ancestor(self, na, nb):
        # check if na is an ancestor of nb
        # returns (logic value of check, list of ancestors)
        val = False
        ancestors = []
        p = nb
        while p is not None:
            if na == p:
                val = True
            ancestors.append(p)
            p = p._parent
        ancestors.reverse()
        return (val, ancestors)

    def post_process(self):
        if (self.best_prune is not None and
            self.best_expansion is not None and
            self.best_expansion.mem_val < self.best_prune.mem_val):

            # consider replacing a subtree for a fringe
            n_exp = self.best_expansion.node
            n_prune = self.best_prune.node

            (is_ancestor, ancestors) = self.is_ancestor(n_prune, n_exp)  # can't prune the ancestor of the expansion
            if is_ancestor:
                self.best_prune = None
                self.best_expansion = None
            else:
                # first we need to update the value of the best expansion
                for n in ancestors:
                    n._rho_update()
                ll_val_exp = n_exp.get_expand_val()
                n_exp.ll_val = ll_val_exp
                self.best_expansion.ll_val = ll_val_exp

                ll_cost_prune = self.best_prune.ll_val
                if ll_val_exp*ll_cost_prune > self.expand_lvl:  # net LL increase
                    logging.debug('Pruning %s (Cost: %.2f)' % (n_prune._uid, ll_cost_prune))
                    dsize = n_prune.prune_children()
                    self.node_count -= self.best_prune.mem_val
        self.best_prune = None  # must always be reset

        if self.best_expansion is not None and self.get_available_size() > self.best_expansion.mem_val:
            n_exp = self.best_expansion.node
            ll_val_exp = self.best_expansion.ll_val  # no update necessary in this case (rhos already up-to-date)
            logging.debug('Expanding fringe at %s (Value: %.2f)' % (n_exp._uid, ll_val_exp))
            try:
                n_exp.expand_fringe(self.best_expansion.mem_val)
            except:
                logging.warn("Fringe expansion failed.")
            self.fringe_node_count -= self.best_expansion.mem_val
            self.best_expansion = None

    def print_tree(self, filepath):
        self.get_root().update_uid_full(max_depth=core.MAX_PRINT_DEPTH)
        super(UCTW, self).print_tree(filepath)

    def get_utile_mem_length(self):
        return self._height + self.max_fringe_depth

    def get_node_count(self):
        return self.node_count - self.fringe_node_count

    def get_minimum_mem_length(self):
        return self.max_fringe_depth

    def build_pydot_dag(self, dag, node, idn, label='', in_fringe=False, print_fringe=False):
        if node.get_depth() > core.MAX_PRINT_DEPTH:
            return

        if node.is_frontier:
            in_fringe = True

        if not print_fringe and in_fringe:
            return

        exp = node.get_total_exp()
        logp = 1.0 if exp == 0 else node.get_estimator().get_seq_logprob(node.get_counts(), exp)
        full_label = ('<<FONT POINT-SIZE="8">l = [%.2f]<BR/>count = %d<BR/>r:%.2f, b:%.2f<BR/>db:%.2f</FONT>>'
                      % (logp, node.get_total_exp(), node._rho, node._beta, node._dbeta))

        if node._beta >= core.MAX_EXPONENT or in_fringe:
            betaval = 0
        elif node._beta <= 0:
            betaval = 255
        else:
            betaval = int(255 - np.exp(np.log(255)*node._beta/core.MAX_EXPONENT))

        n = pydot.Node(idn,
                       label=full_label,
                       style="filled",
                       color="black",
                       fillcolor="#0000%2x" % betaval,
                       fontcolor="white")
        dag.add_node(n)

        visited_nodes = set([])
        rhoval = int(node._rho * 255)
        for (m, c) in node.get_children():
            if c not in visited_nodes:
                visited_nodes.add(c)
                if c.is_frontier and not print_fringe:
                    continue
                new_idn = c.get_uid()
                label = c.get_label()
                edgecolor = "#00%2x00" % rhoval if (in_fringe or c.is_frontier) else "#%2x0000" % rhoval
                edgestyle = "dashed" if (in_fringe or c.is_frontier) else "solid"
                e = pydot.Edge(idn,
                               new_idn,
                               label=label,
                               color=edgecolor,
                               style=edgestyle,
                               penwidth=2 * ((rhoval / 255.0) + 0.5))
                dag.add_edge(e)
                self.build_pydot_dag(dag, c, new_idn, in_fringe=in_fringe)

    def get_height(self):
        """DFS Search -- use with care"""
        nodelist = [self._root]
        maxdepth = 0
        while len(nodelist) > 0:
            n = nodelist.pop()
            if n.is_frontier:
                continue

            d = n.get_depth()
            if d > maxdepth:
                maxdepth = d
            nodelist.extend(n._children.values())
        return maxdepth
