import numpy as np
import logging
import pydot

import vmm_py.modelling.core as core
from vmm_py.modelling.core import ContextTreeBuilder, PredictorNode


class CTWNode(PredictorNode):
    def __init__(self, estimator, builder, parent=None, label=None):
        super(CTWNode, self).__init__(estimator, builder, parent, label)
        self._beta = 0.

    def update_weighted_prob(self, y, stm, add_exp=True):
        """
        We need to compute:
        P^n_w = 0.5*(P_e + prod(P^c_w))
        This can be very small, so we'll use the log to make it more robust to underflows
        (same trick as in J. Veness' code)
        <=> P^n_w = 0.5*P_e*(1 + prod(P^c_w)/P_e)
        <=> log2(P^n_w) = log2(0.5) + log2(P_e) + log2(1 + prod(P^c_w)/P_e)
        If sum(log2(P^c_w)) - log2(P_e) is large, then this is roughly the same as:
        <=> log2(P^n_w) ~= -1 + log2(P_e) + log2(prod(P^c_w)) - log2(P_e)
        <=> log2(P^n_w) ~= sum(log2(P^c_w)) - 1
        """
        ny = self.get_count(y)
        pe_cond = self.get_estimator().get_prob(ny, self.get_total_exp(), self.get_ab_size())
        delta_log_pe = np.log2(pe_cond)

        d = self.get_depth()
        if d >= len(stm):
            delta_log_pw = delta_log_pe
        else:
            try:
                m = stm[d]
                if (not (self._builder.node_count >= self._builder.max_nodes > 0) and
                    m not in self._children and add_exp):
                    v = self.add_child(m)
                if m in self._children:
                    c = self._children[m]
                    delta_log_pw_children = c.update_weighted_prob(y, stm, add_exp)
                else:
                    raise IndexError
                beta_prev = self._beta
                if add_exp:
                    self._beta += delta_log_pw_children - delta_log_pe
                    beta = self._beta
                else:
                    beta = self._beta + delta_log_pw_children - delta_log_pe
                if beta <= -core.MAX_EXPONENT:
                    delta_log_pw = delta_log_pe
                elif beta > core.MAX_EXPONENT:
                    delta_log_pw = delta_log_pw_children
                else:
                    f = (1 + 2**beta)/(1 + 2**beta_prev)
                    delta_log_pw = delta_log_pe + np.log2(f)
            except IndexError:
                delta_log_pw = delta_log_pe
        if add_exp:
            self.add_exp(y)

        return delta_log_pw


class CTW(ContextTreeBuilder):
    def __init__(self, params, nodetype=CTWNode):
        super(CTW, self).__init__(params,
                                  nodetype)

    def train(self, y, stm):
        if len(stm) < self.get_minimum_mem_length():
            loss = -np.log2(1.0/self.get_default_ab_size())
        else:
            loglikeh = self.get_root().update_weighted_prob(y, stm, add_exp=True)
            loss = -loglikeh
            assert(loss >= 0)
        self._total_exp += 1
        return loss

    def test(self, y, stm):
        if len(stm) < self.get_minimum_mem_length():
            loss = -np.log2(1.0/self.get_default_ab_size())
        else:
            loglikeh = self.get_root().update_weighted_prob(y, stm, add_exp=False)
            loss = -loglikeh
            assert(loss >= 0)
        return loss

    def get_minimum_mem_length(self):
        return self._max_depth

    def build_pydot_dag(self, dag, node, idn, label=''):
        full_label = ('<<FONT POINT-SIZE="8">count = %d<BR/>b:%.2f</FONT>>'
                      % (node.get_total_exp(), node._beta))

        if node._beta >= 50:
            betaval = 0
        elif node._beta <= 0:
            betaval = 255
        else:
            betaval = int(255 * (1.0 - node._beta/50))
        edgeval = 255 - betaval
        fillc = "#0000%2x" % betaval
        if node._parent is not None and node._parent._beta <= 0:
            fillc = "#A0A0A0"
            edgeval = 0

        n = pydot.Node(idn,
                       label=full_label,
                       style="filled",
                       color="black",
                       fillcolor=fillc,
                       fontcolor="white")
        dag.add_node(n)

        visited_nodes = set([])
        for (m, c) in node.get_children():
            if c not in visited_nodes:
                visited_nodes.add(c)
                new_idn = c.get_uid()
                label = c.get_label()
                edgecolor = "#%2x0000" % edgeval
                edgestyle = "solid"
                e = pydot.Edge(idn,
                               new_idn,
                               label=label,
                               color=edgecolor,
                               style=edgestyle,
                               penwidth=2 * ((edgeval / 255.0) + 0.5))
                dag.add_edge(e)
                self.build_pydot_dag(dag, c, new_idn)
