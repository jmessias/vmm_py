import numpy as np
import logging
import pdb

from vmm_py.modelling.core import PredictorNode, ContextTreeBuilder


class PPMNode(PredictorNode):
    def __init__(self, estimator, builder, parent=None, label=None):
        super(PPMNode, self).__init__(estimator, builder, parent, label)

    def get_total_exp(self, mask=set([])):
        if len(mask) == 0:
            return super(PPMNode, self).get_total_exp()
        else:
            counts = self.get_counts()
            masked_exp = 0
            for y in mask:
                try:
                    masked_exp += counts[y]
                except KeyError:
                    pass
            return self._total_exp - masked_exp

    def get_y_prob(self, y, mask=set([])):
        counts = self.get_counts()
        total_exp = self.get_total_exp()
        for ym in mask:
            try:
                total_exp -= counts[ym]
            except KeyError:
                pass
        try:
            ny = counts[y]
        except KeyError:
            ny = 0

        py = self.get_estimator().get_prob(ny, total_exp, self.get_ab_size() - len(mask))
        return py


class PPMCTree(ContextTreeBuilder):
    """Note: This is a direct suffix-tree implementation of PPM, which is slightly different from
    the standard "trie of subsequences" implementation."""
    def __init__(self, params):
        super(PPMCTree, self).__init__(params,
                                       PPMNode)
        try:
            self.min_exp = params['min_exp']
        except KeyError:
            self.min_exp = 10
        logging.info('Minimum node experience count: %d' % self.min_exp)

        if params['estimator'] != 'FREQ':
            logging.warning("==== PPM with estimators other then 'FREQ' performs very poorly! ====")

    def get_logloss(self, y, stm):
        if self._total_exp == 0:
            return -np.log2(1.0/self.get_default_ab_size())
        n = self.match_stm(stm)
        prior_p = 1.0
        p_y = None
        mask = set([])

        while n is not None:
            ab_size = n.get_ab_size() - len(mask)
            if ab_size > 0:
                p_escape = n.get_y_prob(None, mask)
                if n.get_count(y) > 0:
                    p_y = prior_p*n.get_y_prob(y, mask)
                    break
            else:
                p_escape = 1.0
            mask = mask.union(n.get_counts().keys())
            n = n.get_parent()
            prior_p *= p_escape

        if n is None:
            p_y = prior_p*(1.0/(self.get_default_ab_size() - len(mask)))

        return -np.log2(p_y)

    def update(self, y, stm):
        n = self._root
        n.add_exp(y)
        for m in stm:
            try:
                if not (n.is_deterministic() and n.get_total_exp() > self.min_exp):
                    n = n.get_child(m)
                    n.add_exp(y)
                else:
                    break
            except KeyError:
                if (not (n.get_depth() > self._max_depth > 0) and
                    not (self.node_count >= self.max_nodes > 0)):
                    n.add_child(m)
                    n = n.get_child(m)
                    n.add_exp(y)
                else:
                    break

        self._max_depth = max([self._max_depth, n.get_depth()])
        self._total_exp += 1

    def train(self, y, stm):
        loss = self.get_logloss(y, stm)
        self.update(y, stm)
        return loss

    def test(self, y, stm):
        return self.get_logloss(y, stm)

