#!/usr/bin/env python
import numpy as np
import yaml
import socket
import pdb
import pickle
from argparse import ArgumentParser
from po_gym.utils.stream_io import StreamClient

np.seterr(all='raise')

class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

getch = _GetchUnix()

def main():
    parser = ArgumentParser(description='Run a VMM agent and transmit actions / observations / rewards via as'
                                        'bitstrings through a socket.')
    parser.add_argument('env', metavar='ENV_NAME',
                        type=str,
                        help='The name of the environment')
    parser.add_argument('--env-params',
                        type=str,
                        dest='env_params',
                        default='env_params.yaml',
                        help="Path to a YAML file containing environment parameters. Default is 'env_params.yaml'")
    parser.add_argument('-H', '--host',
                        type=str,
                        dest='host',
                        default='localhost',
                        help='The hostname to use in the stream server. Default is localhost.')
    parser.add_argument('-p', '--port',
                        type=int,
                        dest='port',
                        default=8096,
                        help='The port to use for communication with the agent. Default is 8096')
    parser.add_argument('-T', '--timeout',
                        type=float,
                        dest='timeout',
                        default=5.0,
                        help='The timeout to use during socket read/write operations, in seconds. '
                             'Default is 5.0 seconds.')

    args = parser.parse_args()

    with open(args.env_params) as env_params_file:
        env_params_dict = yaml.load(env_params_file)
    env_params = env_params_dict[args.env]

    a_bits = max([int(np.ceil(np.log2(env_params['number_of_actions']))), 1])
    o_bits = max([int(np.ceil(np.log2(env_params['number_of_observations']))), 1])
    min_r = min(env_params['reward_space'])
    delta_r = max(env_params['reward_space']) - min_r
    r_bits = int(np.floor(np.log2(delta_r)) + 1)

    stream_client = StreamClient(a_bits, o_bits, r_bits,
                                 args.host, timeout=args.timeout, port=args.port)
    R_hist = []
    finished = False
    t = 0
    while not finished and t < env_params['timestep_limit']:
        try:
            (o, r) = stream_client.recv_env_info()
            print 'Observation:', o
            print 'Reward:', r + min_r
            a = int(getch())
            stream_client.send_action(a)
            print 'Selected action:', a
            R_hist.append(r + min_r)
            print 'Average reward/step (1000 steps): %.2f' % np.mean(R_hist[-200:])
        except socket.error:
            finished = True

if __name__ == '__main__':
    main()
