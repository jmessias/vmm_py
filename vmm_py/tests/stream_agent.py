#!/usr/bin/env python
import numpy as np
import yaml
import socket
import time
import pickle
from argparse import ArgumentParser
from vmm_py.tests.vmm_agent import VMMAgent
from po_gym.utils.stream_io import StreamClient
import logging
import os

np.seterr(all='raise')

def main():
    parser = ArgumentParser(description='Run a VMM agent and transmit actions / observations / rewards via as'
                                        'bitstrings through a socket.')
    parser.add_argument('env', metavar='ENV_NAME',
                        type=str,
                        help='The name of the environment')
    parser.add_argument('alg', metavar='ALG',
                        choices=['ML', 'PPMC', 'CTW', 'UCTW'],
                        help='The modelling algorithm to use.')
    parser.add_argument('--env-params',
                        type=str,
                        dest='env_params',
                        default='env_params.yaml',
                        help="Path to a YAML file containing environment parameters. Default is 'env_params.yaml'")
    parser.add_argument('-S', '--nsimulations',
                        type=int,
                        dest='num_simulations',
                        default=50,
                        help='Number of Monte Carlo simulations. Default is 50.')
    parser.add_argument('-M', '--max-mem',
                        type=float,
                        dest='max_mem',
                        default=np.Inf,
                        help='Maximum amount of memory to use in the VMM model. Default is inf.')
    parser.add_argument('-g', '--discount',
                        type=float,
                        dest='discount',
                        default=1.0,
                        help='Discount factor to use in Monte Carlo simulations. Default is 1.0')
    parser.add_argument('-hz', '--horizon',
                        type=float,
                        dest='horizon',
                        default=np.Inf,
                        help='Horizon to use in Monte Carlo simulations. Default is inf.')
    parser.add_argument('-C', '--exp-c',
                        type=float,
                        dest='exp_c',
                        default=1.0,
                        help='Exploration/Exploitation tradeoff const for UCT. Default is 1.0')
    parser.add_argument('-mt', '--threads',
                        type=int,
                        dest='num_threads',
                        help='Number of threads to use in UCT. Default is 1.')
    parser.add_argument('-c', '--cache',
                        action='store_true',
                        dest='cache_trees',
                        help='Cache the UCT trees (may require tons of memory).')
    parser.add_argument('-eps', '--epsilon',
                        type=float,
                        dest='epsilon',
                        default=1.0,
                        help='Rate of random actions to use (Epsilon-greedy). Default is 0.')
    parser.add_argument('-dec', '--eps-decay',
                        type=float,
                        dest='epsilon_decay',
                        default=1.0,
                        help='Decay for the epsilon parameter. Default is 1.0 (No decay).')
    parser.add_argument('-H', '--host',
                        type=str,
                        dest='host',
                        default='localhost',
                        help='The hostname to use in the stream server. Default is localhost.')
    parser.add_argument('-p', '--port',
                        type=int,
                        dest='port',
                        default=8096,
                        help='The port to use for communication with the agent. Default is 8096')
    parser.add_argument('-T', '--timeout',
                        type=float,
                        dest='timeout',
                        default=5.0,
                        help='The timeout to use during socket read/write operations, in seconds. '
                             'Default is 5.0 seconds.')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        dest='verbose',
                        help='Run in verbose mode, printing actions, observations, and rewards.')
    parser.add_argument('-L', '--log',
                        type=str,
                        dest='log_level',
                        default='info',
                        choices=['info', 'warn', 'debug', 'error', 'critical'],
                        help="The logging severity level. Default is 'info'.")
    parser.add_argument('-LF', '--log-to-file',
                        action='store_true',
                        dest='log_to_file',
                        help="Write the log to a file. By default, the log goes"
                             "to the standard output.")
    parser.add_argument('--random',
                        action='store_true',
                        dest='random',
                        help="Use a random agent (useful to get a baseline).")
    parser.add_argument('-z', '--noise',
                        type=float,
                        dest='noise',
                        default=0.0,
                        help='The noise level (in [0, 1]). Default is 0.')
    parser.add_argument('-d', '--max-depth',
                        type=int,
                        dest='max_depth',
                        default=5,
                        help='[CTW / PPM] The maximum depth of the predictor. Default is 5.')
    parser.add_argument('-x', '--min-exp',
                        type=int,
                        dest='min_exp',
                        default=10,
                        help='[UCTW / PPM] The minimum count before node operations are enabled. Default is 10.')
    parser.add_argument('-sl', '--split-level',
                        type=float,
                        dest='split_lvl',
                        default=1.0,
                        help='[UCTW] The likelihood ratio at which fringe nodes are considered significant. Default is 1.0.'
                             'Should be >= 1.')
    parser.add_argument('-f', '--fringe-depth',
                        type=int,
                        dest='max_fringe_depth',
                        default=2,
                        help='[UCTW] The maximum depth of the fringe.')
    parser.add_argument('-np', '--no-pruning',
                        action='store_false',
                        dest='enable_pruning',
                        help='[UCTW] Disable pruning operations.')
    parser.add_argument('-e', '--estimator',
                        type=str,
                        dest='estimator',
                        default='SAD',
                        choices=['FREQ', 'SAD', 'SADM', 'KT'],
                        help='The estimator to use. Default is SAD.')
    parser.add_argument('-N', '--max-nodes',
                        type=int,
                        dest='max_nodes',
                        default=-1,
                        help='The maximum number of nodes in the tree. Default is 0 (disabled).')
    parser.add_argument('-o', '--ofdir',
                        type=str,
                        dest='ofdir',
                        default=None,
                        help='The output directory.')
    parser.add_argument('-u', '--uuid',
                        type=str,
                        dest='uuid',
                        default=None,
                        help='An unique identifier for this experiment. Defaults to the current time.')
    
    args = parser.parse_args()
    datestr = time.strftime("_%d_%b_%Y_%H_%M_%S", time.gmtime())
    suffix = args.uuid if args.uuid is not None else datestr
    log_level = getattr(logging, args.log_level.upper(), None)
    if not isinstance(log_level, int):
        raise ValueError('Invalid log level: %s' % log_level)

    write_results = args.ofdir is not None and os.path.isdir(args.ofdir)
    if write_results:
        if args.ofdir[-1] != '/':
            args.ofdir += '/'
        ofprefix = args.ofdir + args.env + '_' + args.alg + '_' + suffix
    else:
        ofprefix = ''

    if args.log_to_file:
        log_file = ofprefix + 'stream_agent_%s.log' % datestr
    else:
        log_file = ''
    logging.basicConfig(filename=log_file, filemode='w+', level=log_level)
    logging.warn("%s: New stream_agent test starting." % datestr)


    with open(args.env_params) as env_params_file:
        env_params_dict = yaml.load(env_params_file)
    env_params = env_params_dict[args.env]
    alg_params = {'alg': args.alg,
                  'max_depth': args.max_depth,
                  'min_exp': args.min_exp,
                  'split_lvl': args.split_lvl,
                  'max_fringe_depth': args.max_fringe_depth,
                  'enable_pruning': args.enable_pruning,
                  'estimator': args.estimator,
                  'max_nodes': args.max_nodes,
                  'ab_size': len(env_params['reward_space'])*env_params['number_of_observations']}
    if not args.random:
        agent = VMMAgent(alg_params,
                         env_params,
                         num_simulations=args.num_simulations,
                         exp_c=args.exp_c,
                         discount=args.discount,
                         horizon=args.horizon,
                         cache_trees=args.cache_trees)

    a_bits = max([int(np.ceil(np.log2(env_params['number_of_actions']))), 1])
    o_bits = max([int(np.ceil(np.log2(env_params['number_of_observations']))), 1])
    delta_r = max(env_params['reward_space']) - min(env_params['reward_space'])
    r_bits = int(np.floor(np.log2(delta_r)) + 1)

    stream_client = StreamClient(a_bits, o_bits, r_bits,
                                 args.host, timeout=args.timeout, port=args.port)
    R_hist = []
    min_r = min(env_params['reward_space'])
    finished = False
    t = 0
    eps = args.epsilon
    while not finished and t < env_params['timestep_limit']:
        try:
            rv = np.random.rand()
            if rv < args.noise:
                o = np.random.choice(env_params['number_of_observations'])
                r = np.random.choice(env_params['reward_space']) - min_r
            else:
                (o, r) = stream_client.recv_env_info()
            rv = np.random.rand()
            if args.random or rv < eps:
                a = np.random.choice(env_params['number_of_actions'])
            else:
                a = agent.act(o, r)
            stream_client.send_action(a)
            r_eff = r + min_r
            R_hist.append(r_eff)
            if args.verbose:
                print '=== Stream Agent stepped (step %d) ===' % t
                print 'Observation:', o
                print 'Reward:', r_eff
                print 'Selected action:', a
                print 'Average reward/step (1000 steps): %.2f' % np.mean(R_hist[-200:])
                print 'Epsilon:', eps
                t += 1
            if eps > 0:
                eps *= args.epsilon_decay
        except socket.error:
            finished = True
    print '=== Simulation finished ==='
    stats = {'size': agent.get_model_size(),
             'height': agent.get_model_height()}
    print 'Model size:', stats['size']
    print 'Model height:', stats['height']

    if write_results:
        with open(ofprefix + '_rewards.pkl', 'w+') as of:
            pickle.dump(R_hist, of)

        with open(ofprefix + '_params.yaml', 'w+') as of:
            yaml.dump(args.__dict__, of)

        with open(ofprefix + '_stats.yaml', 'w+') as of:
            yaml.dump(stats, of)


if __name__ == '__main__':
    main()
