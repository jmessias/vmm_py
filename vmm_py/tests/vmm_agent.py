import numpy as np
from vmm_py.planning import uct
from vmm_py.common import memory
from vmm_py.modelling import max_likelihood, ppm, ctw, uctw
from copy import copy
from collections import deque
import pdb


alg_map = {'ML': max_likelihood.MLMarkov,
           'PPMC': ppm.PPMCTree,
           'CTW': ctw.CTW,
           'UCTW': uctw.UCTW}


class VMMAgent(object):
    def __init__(self,
                 alg_params,
                 env_params,
                 num_simulations=50,
                 max_mem=50,
                 exp_c=1.0,
                 discount=1.0,
                 horizon=np.Inf,
                 num_threads=1,
                 cache_trees=False):
        alg = alg_map[alg_params['alg']]
        self.model = alg(alg_params)
        rwd_delta = max(env_params['reward_space']) - min(env_params['reward_space'])
        self.planner = uct.UCTPlanner(sample_fn = self.model.sample,
                                      num_actions=env_params['number_of_actions'],
                                      num_simulations=num_simulations,
                                      rwd_delta=rwd_delta,
                                      exp_c=exp_c,
                                      discount=discount,
                                      horizon=horizon,
                                      num_threads=num_threads,
                                      cache_trees=cache_trees)
        # todo: max_mem is a prop of the model
        self.max_mem = max_mem
        self.history = deque()

    def act(self, o, r):
        y = (o, r)
        self.model.train(y, self.history)
        a = self.planner.plan(y, self.history)
        self.history.appendleft((y, a))
        if len(self.history) > self.model.get_utile_mem_length():
            self.history.pop()

        return a

    def get_model_size(self):
        return self.model.get_node_count()

    def get_model_height(self):
        return self.model.get_height()