#! /usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import yaml
import os
import subprocess
import pdb

base_dir = 'results/ctw/1.0/'
with open(base_dir + 'best_params.yaml', 'r') as f:
    best_params = yaml.load(f)

ld = os.listdir('/home/joao/calgary')
for l in ld:
    with open(base_dir + best_params[l], 'r') as f:
        file_params = yaml.load(f)
        subprocess.call(['./predictor_test.py',
                         '/home/joao/calgary/%s' % l,
                         'CTW',
                         '-d %d' % file_params.max_depth,
                         '-LF',
                         '-s 1.0',
                         '-z 0.1',
                         '-N 1000',
                         '-o/home/joao/Workbench/vmm_py/vmm_py/tests/results/ctw/noisy_1k_best_params'])
