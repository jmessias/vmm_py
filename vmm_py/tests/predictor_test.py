#! /usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import yaml
import pickle
from vmm_py.modelling.uctw import UCTW
from vmm_py.modelling.ctw import CTW
from vmm_py.modelling.ppm import PPMCTree

from collections import deque
import time
import os
import logging
import pdb
from progress.bar import Bar
from argparse import ArgumentParser
from multiprocessing import Manager, Process


np.seterr(all='raise')
algmap = {'PPMC': PPMCTree,
          'CTW': CTW,
          'UCTW': UCTW}


def main():
    parser = ArgumentParser(description='Run a test on a given file '
                                        'with the selected prediction mechanism.')
    req_args = parser.add_argument_group('Required arguments')

    req_args.add_argument('file', metavar='FILE',
                          help='The input file on which to run the predictor.')

    req_args.add_argument('alg', metavar='ALG',
                          help='The algorithm to use: PPMC, CTW or UCTW')

    global_args = parser.add_argument_group('Global arguments')

    global_args.add_argument('-z', '--noise',
                             type=float,
                             dest='noise',
                             default=0.0,
                             help='The noise level (in [0, 1]). Default is 0.')
    global_args.add_argument('-a', '--absize',
                             type=int,
                             dest='ab_size',
                             default=256,
                             help='The alphabet size. Default is 256.')
    global_args.add_argument('-b', '--binary',
                             action='store_const',
                             const=True,
                             dest='binary',
                             help='Read input in binary mode. Overrides -a, and assumes byte'
                             ' oriented input.')
    global_args.add_argument('-p', '--print',
                             action='store_const',
                             const=True,
                             dest='printm',
                             help='Print the tree model after training. Default is False.')
    global_args.add_argument('-s', '--split',
                             type=float,
                             dest='split',
                             default=0.5,
                             help='The training / testing split (in [0, 1]). Default is 0.5.')
    global_args.add_argument('-e', '--estimator',
                             type=str,
                             dest='estimator',
                             default='SAD',
                             choices=['FREQ', 'SAD', 'SADM', 'KT'],
                             help='The estimator to use. Default is SAD.')
    global_args.add_argument('-N', '--max-nodes',
                             type=int,
                             dest='max_nodes',
                             default=-1,
                             help='The maximum number of nodes in the tree. Default is 0 (disabled).')
    global_args.add_argument('-L', '--log',
                             type=str,
                             dest='log_level',
                             default='info',
                             choices=['info', 'warn', 'debug'],
                             help="The logging severity level. Default is 'info'.")
    global_args.add_argument('-LF', '--log-to-file',
                             action='store_true',
                             dest='log_to_file',
                             help="Write the log to a file. By default, the log goes"
                                  "to the standard output.")
    global_args.add_argument('-o', '--ofdir',
                             type=str,
                             dest='ofdir',
                             default='.',
                             help='The output directory.')
    global_args.add_argument('--dry',
                            action='store_true',
                            dest='dry',
                            help='Dry run, without producing any log files.')
    spec_args = parser.add_argument_group('Algorithm-specific arguments')

    spec_args.add_argument('-d', '--max-depth',
                           type=int,
                           dest='max_depth',
                           default=5,
                           help='[CTW / PPM] The maximum depth of the predictor. Default is 5.')
    spec_args.add_argument('-x', '--min-exp',
                           type=int,
                           dest='min_exp',
                           default=10,
                           help='[UCTW / PPM] The minimum count before node operations are enabled. Default is 10.')
    spec_args.add_argument('-sl', '--split-level',
                           type=float,
                           dest='split_lvl',
                           default=1.0,
                           help='[UCTW] The likelihood ratio at which fringe nodes are considered significant. Default is 1.0.'
                                'Should be >= 1.')
    spec_args.add_argument('-f', '--fringe-depth',
                           type=int,
                           dest='max_fringe_depth',
                           default=2,
                           help='[UCTW] The maximum depth of the fringe.')
    spec_args.add_argument('-np', '--no-pruning',
                           action='store_false',
                           dest='enable_pruning',
                           help='[UCTW] Disable pruning operations.')


    args = parser.parse_args()

    if not args.dry and not os.path.isdir(args.ofdir):
        print "ERROR: Specified output directory '%s' does not exist." % args.ofdir
        return
    if args.ofdir[-1] != '/':
        args.ofdir += '/'
    ix = args.file.rfind('/') + 1
    datestr = time.strftime("_%d_%b_%Y_%H_%M_%S", time.gmtime())
    ofprefix = args.ofdir + args.file[ix:] + '_' + args.alg + datestr

    log_level = getattr(logging, args.log_level.upper(), None)
    if not isinstance(log_level, int):
        raise ValueError('Invalid log level: %s' % log_level)
    if args.log_to_file:
        log_file = ofprefix + '_log'
    else:
        log_file = ''
    logging.basicConfig(filename=log_file, level=log_level)

    logging.info("%s: New predictor test starting." % datestr)
    f = open(args.file, 'r')
    nchars = os.stat(args.file).st_size
    M = algmap[args.alg](args.__dict__)
    print_post = args.printm
    binary_mode = args.binary
    block_size = 8 if binary_mode else 1
    test_pmf = False  # for debug
    train_lloss = []
    test_lloss = []
    Ttrain = []
    Ttest = []
    stm = deque()

    """First pass: Training"""
    trainbytes = int(np.floor(nchars*args.split))
    bar = Bar('Training', max=trainbytes/10)
    logging.debug("=== Training phase started. ===")
    for i in xrange(trainbytes):
        logging.debug("== Step %d started ==" % i)
        if np.random.uniform() < args.noise:
            y = chr(np.random.choice(args.ab_size))
        else:
            y = f.read(1)
        if len(y) == 0:  # EOF
            break
        if i % 10 == 0:
            bar.next()
        t0 = time.time()

        if binary_mode:
            loss = 0
            y = ord(y)
            for j in xrange(block_size):
                b = y & 1
                y = y >> 1
                loss += M.train(b, stm)
                stm.appendleft(b)
                if len(stm) > M.get_utile_mem_length():
                    stm.pop()
        else:
            loss = M.train(y, stm)
            stm.appendleft(y)

        train_lloss.append(loss)
        while len(stm) > M.get_utile_mem_length():
            stm.pop()

        t1 = time.time()
        Ttrain.append(t1-t0)

    bar.finish()

    """second pass: Testing"""
    if args.split < 1:
        testbytes = int(np.ceil(nchars*(1-args.split)))
        bar = Bar('Testing', max=testbytes/10)
        logging.debug("=== Testing phase started. ===")
        for i in xrange(testbytes):
            logging.debug("== Step %d started ==" % i)
            if np.random.uniform() < args.noise:
                y = chr(np.random.choice(args.ab_size))
            else:
                y = f.read(1)
            if len(y) == 0:  # EOF
                break
            if i % 10 == 0:
                bar.next()
            t0 = time.time()
    
            if test_pmf:
                sum_p = 0
                for c in xrange(args.ab_size):
                    l = M.test(chr(c), stm)
                    sum_p += 2**(-l)

                if 1.0 - sum_p > 1e-2:
                    pdb.set_trace()
                else:
                    print "SUM P: ", sum_p
    
            if binary_mode:
                loss = 0
                y = ord(y)
                for j in xrange(block_size):
                    b = y & 1
                    y = y >> 1
                    loss += M.test(b, stm)
                    stm.appendleft(b)
            else:
                loss = M.test(y, stm)
                stm.appendleft(y)
    
            test_lloss.append(loss)
            while len(stm) > M.get_utile_mem_length():
                stm.pop()
    
            t1 = time.time()
            Ttest.append(t1-t0)

    f.close()
    bar.finish()
    print('========= FINISHED ==========')
    if args.split > 0:
        print('Average training log-loss: %f' % np.mean(train_lloss))
        print('Median training log-loss: %f' % np.median(train_lloss))
    if args.split < 1:
        print('Average testing log-loss: %f' % np.mean(test_lloss))
        print('Median testing log-loss: %f' % np.median(test_lloss))
        print('Q=.9 testing log-loss: %f' % np.percentile(test_lloss, 90))
        print('Q=.1 testing log-loss: %f' % np.percentile(test_lloss, 10))
        print('Average testing time: %f s' % np.mean(Ttest))
    print('Average training time: %f s' % np.mean(Ttrain))

    # Parallelizable stuff
    mgr = Manager()
    rdict = mgr.dict()

    plist = []
    def get_height(m, r):
        r['height'] = m.get_height()

    plist.append(Process(target=get_height, args=(M, rdict)))
    plist[-1].start()

    if print_post:
        printname = ofprefix + '_model_result.png'
        plist.append(Process(target=M.print_tree, args=(printname,)))
        plist[-1].start()

    for p in plist:
        p.join()

    mheight = rdict['height']
    nnodesstr = 'Number of nodes: %d' % M.get_node_count()
    mheightstr = 'Model height: %d' % mheight

    print(nnodesstr)
    print(mheightstr)

    if not args.dry:
        with open(ofprefix + '_stats.yaml' , 'w+') as of:
            stats_dict = {'number_of_nodes': M.get_node_count(),
                          'height': mheight}
            yaml.dump(stats_dict, of)  # yaml for the human readable params

        with open(ofprefix + '_params.yaml', 'w+') as of:
            yaml.dump(args, of)

        with open(ofprefix + '_training.pkl', 'w+') as of:
            train_dict = {'lloss': np.array(train_lloss),
                          'time': np.array(Ttrain)}
            pickle.dump(train_dict, of)  # pickle for the python-readable data

        with open(ofprefix + '_testing.pkl', 'w+') as of:
            test_dict = {'lloss': np.array(test_lloss),
                         'time': np.array(Ttest)}
            pickle.dump(test_dict, of)

if __name__ == '__main__':
    main()
