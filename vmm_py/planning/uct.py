import numpy as np
import pydot
import logging
import pdb
from abc import abstractmethod, ABCMeta
from copy import copy
from multiprocessing import Process, Lock
from threading import Thread, Lock

class UCTNode(object):
    __metaclass__ = ABCMeta

    def __init__(self, sample_fn, uct_params, parent=None, label=''):
        self._lock = Lock()
        self._lock.acquire()
        self.sample = sample_fn
        self.params = uct_params
        self.num_visits = 0
        self.children = {}  # dict {action -> corresponding chance node}
        self.V = 0
        self.parent = parent
        self.depth = parent.depth + 1 if parent is not None else 0
        self.label = label
        if self.depth == 0:
            self.uid = 'R'
        else:
            self.uid = '%s-%s' % (self.parent.uid, self.label)
        self._lock.release()

    def add_child(self, label, child_type):
        if label in self.children:
            logging.error("UCT:: Replacing already-existing child at node %s!" % self.uid)
        self.children[label] = child_type(self.sample, self.params, parent=self, label=str(label))
        return self.children[label]

    def update_v(self, r):
        self.V = 1/float(self.num_visits + 1)*float(r + self.num_visits*self.V)
        self.num_visits += 1

class DecisionNode(UCTNode):
    def __init__(self, sample_fn, uct_params, parent=None, label=''):
        super(DecisionNode, self).__init__(sample_fn, uct_params, parent, label)
        self._lock.acquire()
        self.unselected_actions = np.random.permutation(range(self.params.num_actions)).tolist()   # pre-randomized for fast selection / pop
        self.best_action = None
        self.best_Q = -np.Inf
        if self.depth == 0:
            for a in xrange(self.params.num_actions):
                self.add_child(a, ChanceNode)
        self._lock.release()

    def simulate(self, y, history):
        logging.debug('Simulation at %s' % self.uid)
        self._lock.acquire()
        if self.parent is not None:
            self.uid = '%s-%s' % (self.parent.uid, self.label)
            self.depth = self.parent.depth + 1  # needs to be updated since root may change

        if self.depth == self.params.horizon or self.params.discount**self.depth < 1e-6:
            self._lock.release()
            return 0

        if self.num_visits == 0:
            self._lock.release()
            r = self.rollout(y, history)  # is a node
            self._lock.acquire()
        else:
            action = self.select_action()
            c = self.children[action]
            history.appendleft((y, action))
            self._lock.release()
            r = c.simulate(history)
            self._lock.acquire()

        self.update_v(r)
        self._lock.release()
        return r

    def select_action(self):
        if len(self.unselected_actions) > 0:
            action = self.unselected_actions.pop()
            logging.debug('Selecting previously unselected action %s' % action)
            if action not in self.children:
                self.add_child(action, ChanceNode)
        else:
            rwd_scaling = (1.0 / (self.params.horizon - self.depth) if self.params.horizon < np.Inf
                           else (1 - self.params.discount))
            rwd_scaling /= self.params.rwd_delta
            Q_best = -np.Inf
            best_actions = []
            for [a, c] in self.children.items():
                if c.num_visits > 0:
                    Q_a = rwd_scaling * c.V + self.params.exp_c * np.sqrt(np.log(self.num_visits) / c.num_visits)  # UCB1
                    if Q_a > Q_best:
                        Q_best = Q_a
                        best_actions = [a]
                    elif Q_a == Q_best:
                        best_actions.append(a)
            if len(best_actions) > 0:
                action = np.random.choice(best_actions)
            else:   # can happen in multi-threaded mode if other threads have picked all actions but haven't returned any value yet
                action = np.random.choice(self.params.num_actions)
        return action

    def get_best_action(self):
        Q_best = -np.Inf
        best_actions = []
        self._lock.acquire()
        for [a, c] in self.children.items():
            if c.V > Q_best:
                Q_best = c.V
                best_actions = [a]
            elif c.V == Q_best:
                best_actions.append(a)
        self._lock.release()
        return np.random.choice(best_actions)

    def rollout(self, y, history):
        logging.debug('Rollout started at %s' % self.uid)
        total_r = 0
        rollout_hist = copy(history)
        discount = 1.0
        t = self.depth
        (o, r) = y  # initial observation
        while t < self.params.horizon and discount > 1e-6:
            a = np.random.choice(self.params.num_actions)
            rollout_hist.appendleft(((o, r), a))
            (o, r) = self.sample(rollout_hist)
            total_r += discount*r
            discount *= self.params.discount
            t += 1
        logging.debug('Rollout complete with reward %f' % total_r)
        return total_r


class ChanceNode(UCTNode):
    def __init__(self, sample_fn, uct_params, parent, label):
        super(ChanceNode, self).__init__(sample_fn, uct_params, parent, label)

    def simulate(self, history):
        self._lock.acquire()
        self.depth = self.parent.depth + 1  # needs to be updated since root may change
        self.uid = '%s-%s' % (self.parent.uid, self.label)

        self._lock.release()
        (o, r) = self.sample(history)
        self._lock.acquire()
        try:
            d = self.children[(o, r)]
        except KeyError:
            d = self.add_child((o, r), DecisionNode)
        self._lock.release()
        reward = r + self.params.discount*d.simulate((o, r), history)
        self._lock.acquire()
        self.update_v(reward)
        self._lock.release()
        return reward


class UCTParams(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__


class UCTPlanner(object):
    def __init__(self, sample_fn, **kwargs):
        self.params = UCTParams(kwargs)
        assert self.params.discount < 1 or self.params.horizon < np.Inf, 'You must set either discount < 1 or horizon < Inf'
        self.sample_fn = sample_fn
        self.root = None
        self.rootmap = {}

    def move_root(self, last_a, y):
        try:
            n = self.root.children[last_a].children[y]
            self.root.children = {}
            self.root = n
            self.root.parent = None
            self.root.depth = 0
            self.root.uid = 'R'
            logging.info('UCT:: Successfully recovered past UCT tree.')
        except KeyError:
            self.root = DecisionNode(self.sample_fn, self.params)

    def plan(self, y, history):
        if self.params.cache_trees:
            try:
                self.root = self.rootmap[y]
            except KeyError:
                self.root = DecisionNode(self.sample_fn, self.params)
                self.rootmap[y] = self.root
        else:
            if self.root is None:
                self.root = DecisionNode(self.sample_fn, self.params)
            else:
                last_a = history[0][1]
                self.move_root(last_a, y)
        logging.info("UCT:: Simulations starting with history %s" % history)
        num_sims = 0
        while num_sims < self.params.num_simulations:
            num_threads = min([self.params.num_threads, self.params.num_simulations - num_sims])
            tvec = []
            for _ in xrange(num_threads):
                th = Thread(target=self.run_simulation, args=(y, history))
                tvec.append(th)
                th.start()
            for t in tvec:
                t.join()
            num_sims += num_threads

        best_action = self.root.get_best_action()
        logging.warn("UCT:: Simulations complete. Selecting action %s with value %f" %
                     (best_action, self.root.V))
        return best_action

    def run_simulation(self, y, history):
        sim_history = copy(history)
        self.root.simulate(y, sim_history)
